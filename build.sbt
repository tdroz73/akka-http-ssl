name := "akka-http-ssl"

version := "0.1"

scalaVersion := "2.13.1"

val akkaVersion = "2.5.26"
val akkaHttpVersion = "10.1.11"

libraryDependencies := Seq(
  "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion
)

enablePlugins(JavaAppPackaging)
enablePlugins(DockerPlugin)

dockerBaseImage := "adoptopenjdk/openjdk13"
dockerExposedPorts := Seq(8443)

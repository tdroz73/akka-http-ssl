package com.eleoptics.http

import java.io.{FileInputStream, InputStream}
import java.security.{KeyStore, SecureRandom}

import akka.actor.ActorSystem
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.{ConnectionContext, Http, HttpsConnectionContext}
import akka.stream.{ActorMaterializer, Materializer}
import com.typesafe.config.{Config, ConfigFactory}
import javax.net.ssl.{KeyManagerFactory, SSLContext, TrustManagerFactory}

object Main extends App {

  implicit val actorSystem: ActorSystem = ActorSystem("akka-https")
  implicit val materializer: Materializer = ActorMaterializer()

  def setupHttps: HttpsConnectionContext = {
    val config: Config = ConfigFactory.load
    // Manual HTTPS configuration
    val password: Array[Char] = config
      .getString("server.cert.password")
      .toCharArray

    val ks: KeyStore = KeyStore.getInstance("PKCS12")
    val certLocation = config.getString("server.cert.path")
    val keystore: InputStream = new FileInputStream(certLocation)

    require(keystore != null, "Keystore required!")
    ks.load(keystore, password)

    val keyManagerFactory: KeyManagerFactory =
      KeyManagerFactory.getInstance("SunX509")
    keyManagerFactory.init(ks, password)

    val tmf: TrustManagerFactory = TrustManagerFactory.getInstance("SunX509")
    tmf.init(ks)

    val sslContext: SSLContext = SSLContext.getInstance("TLS")
    sslContext.init(
      keyManagerFactory.getKeyManagers,
      tmf.getTrustManagers,
      new SecureRandom
    )
    ConnectionContext.https(sslContext)
  }

  val routes: Route = get { complete("Hello World!") }

  val https = setupHttps
  println("Starting up!")
  Http().setDefaultServerHttpContext(https)
  Http().bindAndHandle(routes, "0.0.0.0", 8443, connectionContext = https)

}
